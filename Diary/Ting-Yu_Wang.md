This is an example of how your diary file should be named and how it should be structured. It should be written in Markdown.

This diary file is written by Nina Wang E14069036 in the course Professional skills for engineering the third industrial revolution.

# 2019-02-20 #

* The first lecture was great.
* I am really inspired and puzzled by the exponential growth.
* I don't think exponential growth applies to food production.
* I don't think exponential growth applies to food production.
* I think everyone should follow the [Markdown Syntax](https://www.markdownguide.org/basic-syntax/) and the [international standard for dates and time - ISO 8601](https://en.wikipedia.org/wiki/ISO_8601).

# 2019-02-27 #

* The second lecture was a little borring because I had seen Steven Pinker's TED talk.
* Why are houses becoming more expensive when all technical products are becoming cheaper?
* I am really inspired and puzzled by the exponential growth in data and narrow AI.
* The [Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) is helpful.
* Now I understood that I should write the diary entry for all weeks in this same file.

# 2020-09-17 #

* I noticed some transformation due to COVID-19.
* talked about alternative eletric vehicles ready to overwhelm internal combustion engine vehicles
* I learned how collaboration may help develop win-win situation.
* I watched video about how the world is becoming a better place: health, wealth, safe, happiness and so on.
* I doubted the point of view that the world is becoming safer.

# 2020-09-24 #

* "When facts are false, decisons are wrong."
* I should caltivate the ability to identify what is correct and what is make up.
* Call into question how data is collected.
* Data collected by government is more reliable than those from private.
* I have learned that it would be nice in presentation if we can show the data in the past and to predict the trend for future.

# 2020-10-08 #

* Learned the value of money is trust. By trust, may people create transaction.
* I get to know better how credit plays an important role in marketing. One spend one earn.
* Noticed the productivity growth matters in long run.
* I think the video "How the Economic Machine Works" is helpful for me. Since it helps me gain knowledge of how to get economy grow. 
* Realized there is much space for improvement in order to give a good presentation.

# 2020-10-15 #

* Learned about the development of Taiwan economy.
* Pondered why it is unlikely for young people to buy a house and what are the things we can do to make a difference.
* Talked about the operation of banks which keeps them rich.
* Learned the differences of fascism and nationalism. Hatred is born of ignorance.
* Impressed by Christian Picciolini's talk, having the courage to admit he was wrong and strive to make changes.

# 2020-10-22 #

* Real estate is becoming more and more expensive since it is a relative safe way for investment. Further, the interest payment by people taking loans drives up the bank to profit. 
* Being health is an issue everyone cares about. Exercise can not only makes us more healthy physically but also improve our mental health.
* Fiction stories affect our daily life. We choose to follow some rules even though we know they are not true or correct. Perhaps this is what society taught us, to identify ourselves and to fit in some places where we think we might belong.
* Since I believe in god, I do think he has been watching me that I couldn't fool him. Or else, fooling myself.
* It is more important for people to prevent from getting sick or illness than having high quality medical treatments.

# 2020-10-29 #

* Good to know ways to maintain a healthy life, including develop a regular sleep-wake cycle, have enough sleep, maintain social interaction, and develop an exercise habit.
* Exercise is essential to human life. Exercise may improve both mental and physical health. It has protective effects on brain, which protecting brain from incurable diseases such as Alzheimer.
* For friends under depression, stay by their side and listen to them will give them hope for living, knowing that someone does care about them.
* One may not need to be afraid of having conversation with  depression people will need to take the responsible for their well-being. There is no need to worry about saying something wrong. Keeping company is more important than the words you got to say. For most cases, they feel isolated and just want to have somebody to talk to.
* Suicide is preventable. Try being there and listen carefully to the person who wants to end up his or her life, it is likely that you will be the one to save his life.
* Don't end up life so easily.

# 2020-11-05 #

* In today's course, professor said that it may be a big shock for most of the people when it comes to death because death is ralatively rare nowadays. We sometimes forget it is a natural phenomenon of life.
* My grandma just passed away this week and I somehow feel happy for her to get out of the intubation and suffering.
* Everyone is responsible for their own lives, take good care of ourselves before helping others. There are always unexpected things happening around us, we can never make sure things go right. 
* It is interesting to learn the possible reasons for people to end their first job within two years. Including having problems translating technical knowledge to practice, understanding the power system, and establishing relationships. 
* Tangible rules for a happy and fulfilling life: First, love the real you. Second, fight for what you love. Third, keep good relationships.
* A meaningful life may consists of belonging, purpose, transcendence, and story telling.

# 2020-11-12 #

* Focused on the issues that are controversial, analysis the prons and cons.
* I think grid-scale battery storage is a new trend for battery storage for the reason that it is a cleaner, more efficient, and responisible energy storage.
* Universal basic income system has the advantage that people will not need to worry their life expense. However, regulations and strategies must be clarified so people will believe it works.
* I noticed that the weak will stand as an easy prey of the strong, even in the economic cycle.
* Applying new technology for producing may help reduce the price, and prevent the inflation.

# 2020-11-19 #

* Learned that judges can make laws because people think they are fair and knowledgeable. 
* When verdict, judges look for similar cases and rule in same way unless society has changed. This is essencial for keeping the society balance.
* The election law in Taiwan and America is different. Voting in Taiwan makes sure every votes count. However, for those in America, they collected the votes in states, to guarantee the president is their wise choices but tyranny of the majority.
* To reach consensus, broad disscussion and test for agreement must be made before the decision works out.
* In order for people to stay connected and free, rules are made to keep the society stable. Once a person breaks the law, he or she should get corresponding punishment.

# 2020-11-26 #

* In today's course, we mainly took a deeper look into the topics for the course project. Take my team for example, is grid-scale battery storage the best solution to build a sustainable and reliable electricity grid? In our first presentation, we agreed with it. However,  to consider the situation and resources in Taiwan, we found it hard and could be not environmentally friendly for Taiwan to develop grid scale battery storage. Unless there is a kind of battery that can be made and recycle in a cleaner way and provides cheaper electricity.
* For the Universal Basic Income issue, it is hard to take the experiment into practice because it requires a lot of money and the circumstances are difficult to define. Hence, how to conduct a reliable and plausible experiment for the public to believe the system works out is challenging.
* Choosing news to read and believe is an ability people should possess.
* The value of gold is always high, people keep it to maintain purchasing power.
* The Rome empire collapsed because of its never end expanding and wars. The empire needs huge taxes from people to support the expense. 

# 2020-12-03 #

* In today's course, I realized that every news story is told from a perspective and biased to generate a response. We practiced how to find the stakeholders. We reviewed the news we read, finding that news story is not as liberty as it shoud be.
* According to economist Kate Raworth's Ted speech, I learned something I never thought of. Since we know that when something tries to grow forever, it is not a trend for a healthy, living, and thriving system. Then, why would we imagine that our economies would be the one system that could buck this trend and succeed by growing forever? It's boundaries that unleash our potential, growing economies may not always benefit our society.
* Perhaps the only benefit of mainstream media is that they based on more true stories than fake news.
* We are damaging our living environment, the earth everyday. It is time for us to do something to stop this forever destroying. Despite my effort may be a small step, I wish I can to something to improve our society.
* The shape of progress is always up going which is how we define what good is. However, when resources are depleted, we can no longer protect what we stand for.

# 2020-12-10 #

* After today's course which we agreed on doing something to save the world, I am excited to try out eating on a vegetarian diet three days a week. 
* We have to respect the nature and live together with it, but to deplete the resources. The business model has to change to a more entvironmental way. Or else, no animals including humans will be able to survive on earth.
* The first industrial revelution started from stream power printing and telegragh to stream engine for rail. The second industrial revelution is about telephone, radio, and television. Finally, the third industrial revelution is new converge of communication, energy ,and transportation. We are the fossil fuels people and we have to pay for what we have done.
* Climate change has increasing the chance of floods, summer droughts and wildfires, hurricanes and so on. Sadly, this is the new normal, we must find out a solution to use the less of the earth.
* What are the three factors that drive productivity? Better machines, better workers ,and aggregate efficiency. We have to count in the energy loss, resources wasted since we will never be able to gain exactly the amount that we paid. 

# 201217 Thu #

* After today's course, I realized that if there is something I would like to pursue, I will need to be more open-minded. To try my best to reach out and see what I have never seen. To connect with people I don't want to connect with. 
* Unsuccessful but productive
* I felt productive because I finished all the asignments I planned to do today.
* I will read book and get up early the next day.

# 201218 Fri #

* Successful and unproductive
* I had a conversation with a doctor to find out the unmet needs in Pediatric Cardiology.
* I know myself better by reading books, I will start to take the advice for impoving life into practice.

# 201219 Sat #

* Unsuccessful and productive
* I went home to celebrate my mom's birthday, I felt happy staying with my family.
* I felt productive because I finished the final report for one of my courses.
* I will spend most of my time with my mom tomorrow in order to make her happy.

# 201220 Sun #

* Successful and unproductive
* I had dinner with three mechanical engineers, they shared their experiences with me.
* I felt successful to socailize.
* I will plan my courses for the next semester.

# 201221 Mon #

* Unsuccessful and unproductive
* I watched some of the recommended videos to gain ideas in living successful. 
* I will reduce the time using cellphone.

# 201222 Tue #

* Unsuccessful but productive
* I cleaned up my room and started to pack up for moving.
* I will keep on reading the book and prepare for the final exams.

# 201223 Wed #

* Unsuccessful but productive
* I had a meeting with my teammates, discussing our project in rehabilitation of upper limbs.
* Five rules I think that can make me more successful or productive:
1. Eat healthily and exercise everyday. 
2. Read book everyday.
3. Reach out, connect to people I don't want to connect with.
4. Keep good relationships with my family and friends, and show concern to my elderly grandparents.
5. Check if I am a better person than I was yesterday.

# 2020-12-24 #

* Realized that writing important tasks I completed each day and make small changes from day to day may make myself a more successful and productive person.
* Watched Ted talk about Bhutan, a small country claims to remain carbon neutral for good. However, it takes the consequences of climate change as all the other countries. 

# 201225 Fri #

* Unsuccessful and unproductive
* I took a break and relaxed myself on this special Christmas day.
* I wrote and sent a postcard for my friend in United Kingdom.
* I will keep on studying for the final exams.

# 201226 Sat #

* Unsuccessful but productive
* I prepared for my final exams.
* I had lunch with one of my old friends and this reminds me of the good days we were in Korea.
* Tomorrow, I will go for a walk and read books while preparing for the exams.

# 201227 Sun #

* Unsuccessful but productive
* I prepared for the exams and read two chapters of book.
* Tomorrow, I will keep on studying for the final exams and make plans for the coming holidays.

# 201228 Mon #

* Unsuccessful but productive
* I think I am more productive this afternoon after I went for a walk.
* I will get to bed ealier and get up early the next day.

# 201229 Tue #

* Successful and unproductive
* I went NTHU, Hsinchu to report arrival. I had breakfast and lunch with my cousins.
* I spent the evening with my grandma and grandpa, it makes me feel good.
* I will finish one of my final presentation's slides and keep studying for the final exams.

# 201230 Wed #

* Successful and productive
* I had a final meeting with my teammates in Biodesign. I feel like we are finally getting there.
* I have a final exam tomorrow so I will need myself to be well prepared, including having enough sleep.
* Tomorrow, I will make a summary of the important things happened this year, and start carrying out my 2021 new plans. 

# 2020-12-31 #

* While I was looking for the answers for the last course exam, I noticed some interesting findings. I gained knowledge about the past and how the future may be like. I have learned the quantification of values.

# 2021-01-07 #

* I memorized some numbers of statistics about the things happening around us such as planetary boundaries, future population, automation effect, and so on.
* After these courses, I have become a more self-awareness person. I am now able to come face to face with the tasks or issues in life.
* By preparing for the presentations for every week, I have learned more basic knowledge and I realized that working with data, I can make my arguments more persuasive, the solutions more feasible. 