This diary file is written by Joel Alonso F14077013 in the course Professional skills 
for engineering the third industrial revolution.

## 2019-09-19

* Today's lecture was pretty good despite the technical difficulties.
* I've learned a good lesson today, that we should always ask ourselves
why are we doing something.
* In the past I realized that the price of a product would go down but my assumption
was that it was a consecuence of time passing, in other words that it got old, I was
completely unaware that there was a law that showed the same behavior in every product
as the cumulative value grew up.
* What surprised today as we were watching the video was that the presenter said this 
phrase: "intelectuals hate progress". it was quite shocking to me but I think it has 
some truth behind it.

## 2019-09-26

* Today the professor was another person and I was curious about her background but she didn't say so
* Every group presented this week and it was a little strange that each group was interrupted
because of some PPT errors or not specifing the data source
* The videos were very interesting and after watching them i got an idea for the presentation
of next week

## 2019-10-03

* Today's class was amazing, I went out with lots of things learned and I enjoyed today's topic
* Among all the classes so far this one was the first that after watching the videos I had so
many questions, in a positive way, and this topic was really well explaind through the videos and
the graphs
* I like how the professor handles the class and makes it very active between questions from the
teacher and questions from the students

## 2019-10-17

* On today's lecture many people were very silent and I'm not sure why, maybe the energy of the group
was low today
* My group didn't present this week because we are only two members and it seems more people dropped
the course
* Today's videos were very strong on the message especially the one from Juan Enriquez, it made me 
think twice about the people in the US but it also made me realize that there's a lot of fear, in other
words hatred, towards the unkown

## 2019-10-24

* Today the class was nice because only 1 week after I put that it would be nice to work in groups,the professor already planned to do it
* I think it was a good experience working in super groups but I'm not sure if my supergroup didn't prepare well or the time was somewhat short in my opinion, and the time to present it also was somewhat short
* Today's was interesting for me because I see myself as an active person that likes sports and training, so when I saw that today's topic was this, I was motivated to do the research this time
* Our claim was that exercise is the most transformative thing for your brain

## 2019-10-31

* This week I was absent due to an injury I got in my face so my diary will be based on the videos
* A possible claim for this week may be the suicide rate among black children has doubled in the past 20 years
* This lesson's videos are very deep and have a strong messages, i feel moved and i also want to try harder to improve myself and be able to help others

## 2019-11-07
* Today the class started with a very strong image about living people doing fake funerals and I believe, as professor said, it may work for some but not for everyone and I agree because some people could get 
the wrong idea
* When we watched the second video about forgiveness I was very amazed about how this man forgave the person that ended his son’s life, I personally would have a very difficult time with forgiven someone that 
did something that tragic
* In today’s class we found two claims: 1. “the Tariq Khamisa Foundation was able to cut suspensions and expulsions by 70 percent trough the safe school model” 2. “the suicide rate has been rising around the world, 
and it recently reached a 30-year high in America”

## 2019-11-14
* In today's lecture I saw a different perspective of UK through the documentary presented. It showed the abuse of power that authorities do and how to defend or act in those cases
* I took in consideration how things are in my country and even though there's also a corrupted system and bribes, I can't remember times when police wanted to abuse their power or that they tell civilians to not use
their cellphones to capture the things happening
* After the class I thought to myself we all have gaps and things to change in our countries and as people and I also think that this is a huge thing that we need to do if we want to change the world
## 2019-11-21
* Today the class about millenials was interesting and I remembered some "memes" or funny pictures exagerating this reality and in my opinion some of these are not as true as it seems
* I don't quite get the "millenial" idea because even though I was born in 1998, in my country information didn't come as fast as other countries, so now the new generations are dealing with this phenomenon
## 2019-11-28
* This lecture was very interesting because it showed how history always repeat itself and in the same way with the same consequences but we never learn from them, and by that, it repeats over and over again
* there is a documentary series that said: if we don't know our past, we are condemned to repeat it. I think is very true 
* Finding the news homework is going to be challenging to find something that matches
## 2019-12-05
* In today's lecture I learned about planetary boundaries, a topic I was completely unaware of, in the sense that I didn't know these existed neither that we already surpassed the boundary.
* the boundary we are studying this week is novel entities, even thou it has no clear boundaries I still think is very important to take care of, because just like our catchy phrase, "novel entities is the root of all evil,
if you cut the root, you cut the evil"
* I'm really looking forward next class to see who has the catchiest phrase

### 2019-12-26 Thu
 A. successful and unproductive
 B. I didn't study enough for a quiz so i felt unproductive but we won our soccer matches so i felt succesful
 C. I'll try to avoid distractions
 
### 2019-12-27 Fri
 A. successful and productive
 B. I cleaned my room and did some homework so i felt both successful and productive
 C. I want to be more organized so i'll try to write my tasks down
 
### 2019-12-28 Sat
 A. unsuccessful and unproductive
 B. I missed my opportunity to study because i had rugby match on saturday afternoon and then I had another event
 C. I'll try to get back to my schedule

### 2019-12-29 Sun
 A. unsuccessful and unproductive
 B. I woke up late and realized sunday's rugby match was canceled so i went running and then I finished my homework but didn't study
 C. I'll try to stay focused to study

### 2019-12-30 Mon
 A. unsuccessful and unproductive
 B. I didn't study again so I felt unproductive and unsuccessful
 C. I need to focus more

### 2019-12-31 Tue
 A. unsuccessful and productive
 B. i went to all my classes so i felt productive but i felt unsuccessful because i didn't have a happy new year
 C. I'll try to have a positive mind for tomorrow

### 2020-01-01 Wed
 A. unsuccessful and unproductive
 B. I went to Kaohsiung to try and clear my mind but at the end i just felt hopeless and that i wasted my time
 C. I'll try to use all my energies for finals and then relax on vacations