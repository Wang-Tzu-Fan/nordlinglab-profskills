
This diary file is written by Geyu Ye E14061915 in the course Professional skills for engineering the third industrial revolution.

# 2019-02-20 #

* The first lecture was great.
* I am really inspired and puzzled by the exponential growth.
* I don't think exponential growth applies to food production.
* I don't think exponential growth applies to food production.
* I think everyone should follow the [Markdown Syntax](https://www.markdownguide.org/basic-syntax/) and the [international standard for dates and time - ISO 8601](https://en.wikipedia.org/wiki/ISO_8601).

# 2019-02-27 #

* The second lecture was a little borring because I had seen Steven Pinker's TED talk.
* Why are houses becoming more expensive when all technical products are becoming cheaper?
* I am really inspired and puzzled by the exponential growth in data and narrow AI.
* The [Markdown Cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet) is helpful.
* Now I understood that I should write the diary entry for all weeks in this same file.

# 2020-09-17 #

* I dont think a rational pessimism is bad, cause it forces human to realize the peril.
* The last two minutes refreshed my mind, which is delivering a universal value.
* Personally,world becoming worse or better, mostly depends on human nature, then the wealth,health......

# 2020-09-24 #

* I feeled a lot by the viewpoint that social media affect people to judge the fake news, as the liar will be smited by oceans of netizens.
* this video reminds me of a saying from a famous host WangHan when he was asked about why nowadays the nation variety shows so boring and monotonous，"there exists nice shows,
    however they didn't go along with the common favor of most audience. There are some problems occur to the public aesthetics", he said.
* Equally, the key to judge fake news is the promotion of common sence and konwledge of people, that's exactly why we had better to improve ourselves in any field. 

# 2020-10-8 #

* Today we watched two videos introducing currency and economy, and how the economy machine works.
* The income let persons be able to borrow, andthen what they spend will be the income of others, which is exactly a continuous cycle to create the value.
* Transactions make up the market.
* Among these, credit is the key to run and promote.

# 2020-10-15 #

* We did bad in making the powerpoint. Not only to control the details, but also we should turn the presenting more reliable and visual.
* For fascism or extreme nationalism, I nearly konw nothing about them. But I agree the words goes " Fear grows when we dont understand sth, and if we keep ourseleves from it, 
  sometimes it turns to hatred."
* I dont think the imagined items in our lives are worries. I live in cities so surely I seldom dreamd lions,rainforest，fallings.....And things abstract like money,laws and
  corporations are the product of our social system, nowadays making a strong feeling of real to everyone, which cant be described as a fictional story.

# 2020-10-22 #

* It's good for us to choose the great moive to express our ideas.
* I still believe that cold is caused by the low temperature, which make patients weak to get the cold, though I know the truth. Since it raise my attention to aviod catching cold.
* Reviewd the definition of bubble economy. As the virtual capital overwhelmed the physical economy, with the bubble breaking, the economy shut down.
* A nice experience about pressure relief.
* For a claim, making hypothesis is an approach to prove if it can be konwledge. 

# 2020-10-29 #

* we enjoyed a pleasant coorperation with group 7. From the chat, I realized there are still some shortcomings in my team.
* Depression is horrible. Cause most people didin't value the strangeness at first, paitents with depression are usually harder to trust people, especially strangers.
* Accompanying and listening, while leaving him/her personal space to calm down if need.
* Suicide, in my opinion, is the worst result, as the person abandon himself,love and responsibility.

# 2020-11-05 #

* Please the people you really care about.
* Before to help a person in dilemma, we should take action but with restraint, cause it may influence your life and yourself.
* The presentation of our group goes well. We shared a true story around us, meaningful and inspiring.
* For depression, a strong heart and determined confidence could become your strength to save yourself.
* Personally speaking, dealing every little thing in my daily life properly means a lot to me.  

# 2020-11-12 #

* Our group gave a presentation about if renewable energy is the best way to create electricity. The creation process is tough,
  and we actually didnt do it well. The fact is picture is not suggested to be evidence even it contains data. Data with chart and figure may be better.
* Honestly speaking I used to trust renewable energy must be the best way for human, yet we still cant solve the problem with its source demand and efficiency.
* Leagel fiction is a common image uesd in law system. I dont really uderstand why it says name is also a leagel fiction.
* In the video, the policemans who stop drivers or passerby and create difficulties make me disguested. We understand that law cant be perfect, 
  but I cant accept an awful group who execute the law.

# 2020-11-19 #

* We have a chance to konw the difference of laws between several countries.
* And in some countries, the law is made by different mechanism, parliament.
* My group presented the Election laws in Taiwan and US, which is exactly the popular issue now, and I learn a lot from it.

# 2020-11-26 #

* Obviously the schedule today seems more reasonable as we have enough time to go through the video part.
* Learnt a lot form the video about history and currency.
* Before the presentation I have konwn that we will fail it again because of the little teamwork. What I presented was out of my personal understanding.
* Sometimes 'success' or failure is mattered by the same thingsosfozstresh

# 2020-12-03 #

* My presentation listed the news factor about several countries, then compared China and Australia.
* There is a significant difference in the news attitude between countries holding opposite political views.
* We had a further cooperaion in a big team, however, it made the hard work of those who did not report unrewarded.

# 2020-12-10 #

* We watched a video refer to the third industrial revolution, long but meaningful, which interested me to reach more videos about 4.0.
* Novel entities is a fresh concept in these nine boundaries.
* The debate made this class really different from ones before, helping us get a further comprehension for boundary.
                                                                                           
# 2020-12-18-Fri #

* Successful and productive.
* I felt productive because of finishing one of my final presentations. I felt successful since the teacher thought highly of our final work. 
* I 'm going to determine the schedule of weekend for my final of several lessons.

# 2020-12-19-Sat #

* Successful and unproductive.
* I felt successful because of the inspring moive I watched. I felt unproductive because I turned on the music when working.
* I will try some peaceful songs next time.

# 2020-12-20-Sun #

* Unsuccessful and productive.
* I felt unsuccessful because I cant play 2077. I felt productive since I balanced my time and energy on both friends and study.
* I will raise money for it.

# 2020-12-21-Mon #

* Successful and productive.
* I felt successful because of the pleasant Winter solstice and the efficient teamwork with my mates in another subject. I felt productive as I finished the outline and my part of work the same day.
* I will try to finish all the work left before the new coming.

# 2020-12-22-Tue #

* Unuccessful and productive.
* I felt unsuccessful because my friends earned a lot through funds and stocks while I know nothing about them. I felt productive as I kept the word yesterday.
* I will try to learn about it fromm my friends and parents.
 
# 2020-12-25-Fri #

* Unsuccessful and productive.
* I felt productive because I spent a substantial Christmas. I felt unsuccessful since my report was unprofessional for too much internet resources. 
* It's better to report something more vivid.

# 2020-12-26-Sat #

* Successful and unproductive.
* I felt successful for helping some visitors. I felt unproductive because I stayed up too late yesterday and worked little.
* Enough sleep time is imperative.

# 2020-12-27-Sun #

* Unsuccessful and productive.
* I felt unsuccessful because I have little confidence about the coming final. I felt productive since I prepared a lot.
* Keep easy when in dilemma.

# 2020-12-28-Mon #

* Successful and unproductive.
* I felt successful because I learnt how to edit a living pricture. I felt unproductive as I didnt make reflection.
* It looks more relaxing with a living wallpaper on your desktop.

# 2020-12-29-Tue #

* Successful and productive.
* I felt successful because I wrote a great report for a nice moive. I felt productive for the comprehensive analysis in place.
* Sometimes good course also could make people feel nice.
 
# 2020-12-30-Wed #

* Successful and unproductive.
* I felt successful because I learnt a lot about epic today. I felt unproductive since I seemed to learn nothing today.
* It's hesitating for learning literary knowledge.

# 2020-12-31 #

* The final project will be presented in video form.
* Professer told us why he is teeaching this course.
* As the final coming, next week will be hard, and I'm going to set a schedule suitable for my rest and work.

