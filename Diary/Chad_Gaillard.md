*This diary file is written by Chad Gaillard  (E24067028) in the course Professional Skills for Engineering the Third Industrial Revolution
																	(Fall 2019).*

# 2019-12-19

## 14th Lecture

# 2019/12/19

* A. successful and unproductive 
* B. I felt unprductive  because I did not study or complete any asssignment . I felt succesful becaus I had answered a question in class which helped my  group .
* C. I will stick to my study routine.

## 2019/12/19

* A. successful and productive .
* B. I felt both succcessful and productive because during the experiment class the T.A offfered good instructions which allowed me to finish my lab on time with good results.
* C. I wil always try to finish my lab on time. 


## 2019/12/21

* A. successul but unproductive 
* B.I felt successful because I slept most of the day ,this means I got well deserved rest. Unproductive becuae I did not do any assignements .
* C. Even though I deserve the rest I will allocate to doing constructive work 

## 2019/12/22

* A. productive and successful
* B. I was able to attend my language exchange class also was able to teach an entire unit in a short space of time  with my student grasping the concept.
* C. I will continue trying my best evry time I have language exchange 

##  2019/12/23

* A. unproductive and unsuccessful 
* B. I did nothing that day plus on that day I had an exam all in mandarin 
* C. I will try to find the courage to study when I am feeling down 

## 2019/12/24

* A. productive and sussessful 
* B. I was able to design my final lab circuit and also put in some study time .
* C. I will continue that good habbit 

## 2019/12/25

* A. productive and unsuccessful
* B. I was able to attend two labs one in which my circuit worked and the other it did not work .
* C. I will seek help a lot earlier for complicated projects.
  
  
##  5 Rules to be Successful 
* A. consistency over intensity 
* B. confidence in self " without confidence you are twice defeated in th race of life but with confidence you have won evn before you have started ", Marcus Mosiah Garvey.
* C. willing to take risks " fortune favors the brave" 
* D. make sacrifices 
* E. constantly educate yourself about new ideas and chnges so you can be relevant. learn fom your mistakes 


# 2019-12-20
* A. unproductive but succesful 
* B. I felt unproductive because i did not study or complete any asignments. Successful because I was able to answer a queston 
* in class , this means I made a contribution to my group. 
* C. I will try to stick to my study hours on a daily basis 

##  2019/12/27
* A. productive but unsuccesful
* B. I was productive because I was able to complete my circuit in the alotted time during the lab. 
* C. unsuccessfulbecause although the physical part of the circuit worked when connected to the software it did not work.

##  2019/12/28

* A. productive and successful 
* B. productive because I was able to go out with friends to dream mall to have dinner and have fun.
* C. successful because I was able to complete my chores for the day , also was able to study and complete assignments.


##  2019/12/29

* A. productive and  successful
* B .productive because I was able to meet with my language exchange partner who made a special effort to see since she will be
     travelling for the next two months .
*  c.successful because I was able to speak with my mom who cracked lots of jokes with me . This helped me improve my overall 
    mood . 
    
##  2019/12/30 
* A. unproductive and unsuccessful .
*  B . unproductive because i slept for most of the day and just watched lots of videos 
*  c . unsuccessful because I was not able to complete the tasks set for the day such as studying for the upcomming automatic 
       control engineering exam .
       
##  2019/12/31
  
  *  A. productive and successful 
  *   B. productive because I attended all my classes . Also I was able to stay extra hours as the professor tried to complete 
       the syllabus for the course .
  *   C. successful because I was able to study quite a bit and complete assignments . 
  
##  2020/01/01 
  
*  A. productive and successful 
*  B. productive because I studied whole day for my upcoming exam .Also , I did complete a few assignments .
*  C. successful 

## 2020/01/03

*  A. productive and unsuccessful 
*  B. productive because I was able to complete my assignment in the nick of time .
*  C. unsuccessful because I was not able to complete a seemingly easy exam . 


##  2020/01/04

*  A.productive and succcessful 
*  B. productive because I was able to study for my upcoming electronics exam . 
*  C. successful because I was able to achieve the tasks that I had set myself for the day . 

##   2020/01/05

*  A. productive but unsuccessful 
*  B. productive because I studied during the morning 
*  c. unsuccessful because I had to cancell an apointment with my other laguage exchange partner because she was down witht the 
      flu. 

## 2020/01/06 

*  A.productive and successful 
*  B . productive because I studied some more . Also was able to work a few problems and watched a few videos to clear some 
       misconceptions.
*  C. successful because I was able to acheive my set goals for the day . 

## 2020/01/07
*  A productive and successful 
*  B productive because I was abele to take my exam whcih I  prepared for 
*  C successful because I was happy to be over with this exam since it was stressing me out .

## 2020/01/08

* A. productive and successfull 
* B . productive because I spent the whole day on finishing up projects . I had to spend some time in the computer lab 
      to work on my simulations . Later in the day went back home to complete my electronics lab and final project.
* C. successful because I was able to complete all of my taks. 


##  1 knwow current world demogrphics 

*   The activity in class where to took the exam in advance was a nice one , but at the end taking the exam was a lot of facts 
     to remember. So I suggest Every lecture There is a " DID YOU KNOW ?" segment whwere at least two or three world demographic
     issues are raised , or to save time the questions can be given as homework for the following week . The issues should be 
     disccussed on the level where students would say what their initial thoughta were vs the actual answer tehy found . This 
     is done in order to prevent memory for the exam but allow students to process the information and actually have a fair 
     knowledge of the current world demographics. students can also make diary entries based on their findings .
     
##   2 ability to use demographics to explain engineering needs
*    .This can be used to help students data aquisition and also data analysis . students can be given a mini project to
     complete with guided assistance from the professor . In the data aquisition students can target one country . Teacher
     and students can generate a list of questions to research such : what is the population density? , what is the rate 
     of illiteracy? ,how many people have access to health care ? ' how many people have basic utilities such as water ,
     electricity ?, how well are the cities connnected to the outer districts ? what natural resources are available in that 
     country. These are just a few questions that can be used . After which data analysis can be done to highlight the problems
     of that country based on their demographics . Students can provide engineering solutions based on the natural resources 
     available in the country not forgetting to take the demographics into account. For example a country may not have  that much
     rainfall and depends heavily on am internal river for fresh water. Students can think about new ways of storing water to 
     prevent water shortages in the drry season or find alternative ways to provide fresh deinking water whether it is from the 
     air or from underground wells .
     
##   3 Understand planetary boundaries and their current state

*    . The idea of using  to present real time data such as the Ted Talks I find very interesting . Also The professor 
        stimulating discussion about the topic was also good . I see no other better way to create awareness of the planetary 
         boundaries current state .
         
##    4 Understand how the current economic system  fail to distribute resources 

*     . This could be acheived by first educating students about how the economic system functions . This education can take 
         pace in a series of video lectures which the professor can suggest. STudents ought to watch the videos at home . 
         The videos should not be too long in order to prevent boredom or students missing the point . The professor can
         provie a list of ten questions to be answered. Then the following lecture there can be time for discussion . Instead
         of watching in class then try discussion . The reason being sometimes one must watcg a video more than once in order 
         to understand what is being said . This will ensure students are fully ready to participate in active discussion . 
         After understanding how the system works students can now  analyse current ecnomic situation and how resources are 
         distributed . 
         
##     5.Be familiar with future visisons and their implications , such as artificial intelligence ," personal health ,
##       happiness and society "

*       . This can be achieved by suggesting futuristic science journals and articles for students to read .Also a few videos on 
         AI  and it'simplications . This is quite important and I Think should be dealt with in deatail . Students should
         on this topic . They must state the capabilities of AI in the futre and how it is going to affect different aspects of 
         medical and social life . I Think each group should be given a specific focus . For example one group can focus on how it 
         can be used to cure certain ilnesses . Another can deal with energy efficiency adn so on. Each must also showhow it can 
         impact life in at least one way . Students can show how it is being currently used as well .
         
##    6. Understand how data and engineering enable healthier life 

*      . This can be achieved through videos educating students about a timeline whether it is from the  steam engine to present 
         or from the ancient egyptians to presnt . STudents will be able to observe the changing points in history with each
         engineering innovation and how it changed the life of mankind . With this done students can be given a small task to 
         identify on that timeline when certain things were discovered and how it affected health in general. For example when was 
         penicicilin discovered and the impact that it had life . How did fritz haber prevent food shortage when he discovered the
         haber process . How has the advent of the refrigerator enabled us t live a healtier life . students can also identify 
         current breakthroughs that can lead to healthier life .
         
##      7. Know that social relationships gives a 50 percent increased likelihood  of survival 

*        The professor can find a guest speaker such as a counsellor to give at least a 45 minutetalk on the subject with 20 to      .  
         30 minutses of Qand A . Also a video on the subject mattter can work as well . 
        
##       8. Be familiar  with depression and mental health issues 

*        . This calls for another expert in the field of pschology such as a shrink togive students at least a one hour lecture .
            This lecture should seek to inform students of the tell tale signs of depression and mental health . It should also 
            empower students as to how to deal with friends or family going through this expereience. The idea of videos are good
           but it lacks the interactive part. This is something that lots of persons will face in their life at some point . so 
           having a guest speaker will give students that opportunity ask questions and learn more even about their personal 
           experiences. 
           
##      9. know the optimal algorithm for finding a partner in life 

*        .Students can be given a task to briefly explain how dating websites are created . This is to give students an insight 
          into the math an algorithms used in  creating a dating website . students can have a debate as to whether this algorithm
          is the best or can it be alterrd to acheieve a higher succees rate . Based on the algorithm students can be given a 
          hypothetical situation where there are say ten men and ten women looking for a life long partner .Students can use the 
          data plus a simple algorithm to find out whether there is a match or not . Also a video presentation can be used to 
          compare students findings . 
          
##      10. Develop a custom of questioning claims to avoid fake news 
*           Teacher can lead by example in the first few classes by always questioning the claims of students . Teacher should 
            also inform students why he is always questoning the claim so that students see the relevance.When there is group 
            presentation teacher should encourage other students to question the clais of their colleaugues .
            
##       11.Be able to do basic analysis and interpretation of time series data 

*          . The best way to learn is to be engaged .Teacher can provide videos to shw how time series data is analysed then give
           students in small groups to analyse time series data . For exam provide themwith flight data into Taiwan for one year 
           say 2018 and have them analyse the data according to the specifications given . 
##       12. Experience of Collaborative and problem based learning 

*          . This can be achieved by presenting students in groups with mini projects or problems to generate simople solutuins.
           Before the projects are handed out professor should ensure that a lessson on how to work collaboratively with others 
           is taught . 
##       13. Understand that professional success depends on social skills 
   
            . A video presentation  would be the best option here . This  video should feature prominemt ppl or persosn that
            most students know of or can relate to. In the video these people can share their stories as to how they were able to 
            mve up the ladder based on theie scoial skills . Also the cvideo needs to offer students a few tips as to how to develop
            the necessary social skills to ensure their success in the future . 
            
##        14.know that the culture of the workplace affect performace . 
*            I think  another video would do well in explaining this concepts to students . The video should be one done by a 
              a bussiness manager stating the how the culture affects the output of work environment . Also persons sharing both
              their succcesses and failures due to the type of culture in the workplace 

*          
            
           